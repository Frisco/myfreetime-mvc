<?php
if (!defined('BASEDIR')) exit('<code>Forbidden Access</code>');
?>
<div class="row">
    <div class="jumbotron">
        <h2>My PHP Framework</h2>
        <blockquote>
            This is where you start. This is just a simple and plain PHP Framework. It's not a rocket science.
        </blockquote>
    </div>
</div>