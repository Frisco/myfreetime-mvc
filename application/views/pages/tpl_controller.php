<?php
if (!defined('BASEDIR')) exit ('<code>Forbidden Access</code>');
session_start();

class tpl_controller extends Template
{    
    public function __construct() 
    {
        $tags = array("title" => "This is my first time doing PHP",
        "textonly" => "This is my text",
        "baseref" => BASE_URL, 
        "content" => VIEW."pages/content.php", 
        "breadcrumb" => VIEW."pages/breadcrumb.php",
        "topmarque" => VIEW."pages/top.php",
        "footer" => VIEW."pages/footer.php" );
            $template = TEMPLATE."default.html"; 
        
        parent::__construct($template);
        $this->loadtemplate($tags);
    }

}