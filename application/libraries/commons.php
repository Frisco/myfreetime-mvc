<?php
if (!defined('BASEDIR')) exit('<code>Forbidden Access</code>');

# define your own function here

# function to load javascript
# It's recommended to create separate javascript file for each controller so we can maximize it.
# Put every <controllerName>.js inside the js folder
function loadjs($controller_name = URL_1)
{
    $script_src = "<script src=\"js/{$controller_name}.js\"></script>";
    return $script_src;
}