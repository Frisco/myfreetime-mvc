<?php
if (!defined('BASEDIR')) exit('<code>Forbidden Access</code>');

# Application settings and environment settings
# This file in included during initiliazation of the index file
# PHP MVC version 1.5

# List of system libraries that you want to load here
$apps['system_libraries'] = array('HTML','Security','DB','Data');

# List of controllers, models, and libraries to be loaded.
$apps['libraries'] = array('commons');

# The application template controller. Usually the location is in views/pages/
$apps['tpl_controller'] = "tpl_controller";

# The application route to default controller
$apps['route'] = "welcome";
