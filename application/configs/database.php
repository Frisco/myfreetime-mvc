<?php
if (!defined('BASEDIR')) exit('<code>Forbidden Access</code>');

# Simple database connectivity definition
# Currently only supports mysql,pgsql
$db['type'] = "mysql";
$db['host'] = "localhost";
$db['user'] = "root";
$db['pass'] = "password";
$db['name'] = "steroids";
