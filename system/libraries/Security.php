<?php

if (!defined('BASEDIR')) {
    exit('<code>Forbidden Access</code>');
}

class Security{
       
    // *** do security md5 ***//
    public static function md_hash($string){
        include SYSTEM.'Configs/CoreConfig'.EXT;
        $salt = $coreconfig['md5salt'];
        $hash = "MD5(CONCAT('$string','$salt'))";
        return $hash;
    }
    
    // *** function to filter unwanted characters it only allows alphanumeric and whitespance*** //
    public static function strip_alphanum($string,$value="/[^a-zA-Z0-9\s\@\.]/"){
            return  preg_replace($value, "", $string);    
    }
    
    # return function f_server
    public static function f_server($type="REQUEST_METHOD")
    {
        return filter_input(INPUT_SERVER, $type);
    }
    
    # return function f_post
    public static function f_post($variable)
    {
        return filter_input(INPUT_POST, $variable);
    }
}