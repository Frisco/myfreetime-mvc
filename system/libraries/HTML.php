<?php
class HTML
{         
    function set_field_focus($formname,$fieldname)
    {
        $focus = "<SCRIPT TYPE=\"text/javascript\">document.$formname.$fieldname.focus()</SCRIPT>";
        return $focus;
    }
    
    static function createform($array)
    {
        $form = "\t<form method='POST' role='form'>\n";
        foreach($array as $name => $properties)
        {
            if(is_array($properties))
            {
                $form .= "\t\t<div class='form-group'>\n";
                $form .= "\t\t\t<input name='$name' {$properties[0]}>\n";
                $form .= "\t\t</div>\n";
            }
            else
            {
                $form .= "\t\t<div class='form-group'>\n";
                $form .= "\t\t\t" . $properties;
                $form .= "\t\t</div>\n";
            }
            
        }        
        $form .= "\t</form>\n";
        return $form;
    }
    
    static function select($array)
    {
        $select = "<select";
        if(array_key_exists("properties", $array))
        {
            foreach($array['properties'] as $prop => $val)
            {
                $select .= " " . $prop . "='" . $val . "'";
            }
            $select .= ">\n";
        }
        if(array_key_exists("static_options", $array))
        {
            foreach($array['static_options'] as $label => $optval)
            {                    
                $select .= "\t\t\t\t<option value='$optval'>$label</option>\n";
            }
        }            
        if(array_key_exists("sql_options", $array))
        {
            $total_data = count(array_filter($array['sql_options']['data']));
            $value = $array['sql_options']['value'];
            $label = $array['sql_options']['label'];
            $selected = $array['sql_options']['selected'];
            
            for($i = 1; $i <= $total_data; $i++)
            {
                if($array['sql_options']['data'][$i-1][$value] == $selected)
                {
                    $selected1 = "selected";
                }
                else
                {
                    $selected1 = "";
                }
                $select .= "\t\t\t\t<option value='" . $array['sql_options']['data'][$i-1][$value] . "'>" . $array['sql_options']['data'][$i-1][$label] . "</option>\n";
            }
        }
        $select .= "\t\t\t</select>\n";
        return $select;
    }
}
