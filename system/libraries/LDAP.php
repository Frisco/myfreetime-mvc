<?php
if (!defined('BASEDIR')) exit('<code>Forbidden Access</code>');

# LDAP Class to connect to LDAP Server

class LDAP
{
    function __construct()
    {
        $this->ldapConnect();
    }
    
    # fuction to do LDAP connection and get config from ldap.php configuration file
    function ldapConnect()
    {
        include CONFIG."ldap.php";
        $ldap_server = $ldap['host'];
        $ldap_port = $ldap['port'];
        $ldap_conn = ldap_connect($ldap_server,$ldap_port) or die("Failed to connect to [$ldap_server] on port $ldap_port");
        return $ldap_conn;
    }
}
?>
