<?php

if (!defined('BASEDIR'))
    exit('<code>Forbidden Access</code>');

//** function to set session
function SetSession($sessionname,$variable='')
{
    if(is_array($sessionname)){
        foreach ($sessionname as $name=>$value)
        {
            $_SESSION[$name] = $value;
        }
    }
    else {
        $_SESSION[$sessionname] = $variable;
    }
}

    /*** set timeout session ***/
/*** required session_start() to be implement at top row ***/
function user_timeout($seconds,$logouturl)
{
    if(isset($_SESSION['timeout']) ) {
	$session_life = time() - $_SESSION['timeout'];
	if($session_life > $seconds)
        { session_destroy(); header("Location:".$logouturl); }
    }
    $_SESSION['timeout'] = time();
}
?>
