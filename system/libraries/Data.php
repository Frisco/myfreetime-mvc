<?php
if(! defined ('BASEDIR')) exit ('<code>Forbidden Access</code>');

/*
 * @Function : Generate grid display and links
 */

function LoadGrid($column,$class='',$extra='')
{
    $class = ""?"":"id=$class";
    echo "<table $class $extra>";
    tHeader($column);
    echo "</table>";
}

/*
 * @function : Generate header for table
 */
function tHeader($value = array(),$align='',$style='')
	{
		echo "<tr>\n";
		foreach ($value as $header)
		{
			echo "<th align='$align' $style>$header</th>\n";
		}
		echo "</tr>\n";
	}

function GridView($dataset,$columns,$hyperlink='',$counter=0,$tablewidth='')
{
    $t = new Table();
    if($tablewidth!=''){ $width="width='$tablewidth'"; } else { $width=''; }
    $t->tbl(1,0,0,0, "id='datatable' $width\n");
    $t->tRow('open');
    if($counter!=0)
    {
        echo "<th class='theader'>No</th>";
    }
    foreach($columns as $col=>$datacol)
    {
        echo "<th class='theader'>$col</th>";
    }
    $t->tRow();
    $count = 1;
    while($row = mysql_fetch_array($dataset))
    {
        $all[] = $row;
        $color = ($count%2)?"#F4F4F4":"#FFF";
        $t->tRow('open','left',"style='background-color: $color'");
        if($hyperlink)
        {
            if($counter==0)
            {
                foreach($columns as $col=>$datacol)
                {
                    $t->tCol("<a href=$hyperlink=$row[$datacol]>".$row[$datacol]."</a>");
                }
            }
            else
            {
                $t->tCol($count);
                foreach($columns as $col=>$datacol)
                {
                    $t->tCol("<a href=$hyperlink=".$row[$datacol[0]].">".$row[$datacol]."</a>");
                }
            }
        }
        else
        {
            if($counter==0)
            {
                foreach($columns as $col=>$datacol)
                {
                 $t->tCol($row[$datacol]);
                }
            }
            else
            {
                $t->tCol($count);
                foreach($columns as $col=>$datacol)
                {
                $t->tCol($row[$datacol]);
                }
            }
        }
        $t->tRow();
        $count++;
    }
    $t->tbl();
}

//field generator
function create_field($category,$dataset,$table='')
{
    while($row = mysql_fetch_array($dataset))
    {
        $field .= "<label>".$row['field_label']."</label>";
        $field .= "<input type='".$row['field_type']."' name='".$row['field_name']."' size='".$row['field_size']."' ".$row['parameters'].">";
        $field .= "<br>";
     }
     return $field;
}

//field generator2
function create_input($data=array(),$parameters="")
{
    foreach($data as $value=>$text)
    {
        $field .= "<li>";
        $field .= "<input ";
        foreach ($value as $val)
        {
            $field .= "$val = '$text'";
        }
        $field .= ">";
        $field .= "</li>";
    }
    return $field;
}

// return row
function SQLReturnRows($resultset)
{
    return mysql_num_rows($resultset);
}

function SQLReturnRow($table,$parameters='',$debug=false)
{
    if($table != NULL)
    {
        if($parameters)
        {
            $Q = "SELECT * FROM $table $parameters";
        }
        else
        {
            $Q = "SELECT * FROM $table";
        }
        
        if($debug==TRUE)
        {
            setDebugMsg("SQL Query : $Q");
        }
        else
        {   
            $result = mysql_query($Q);
            if($result)
            {
                $rowcount = mysql_num_rows($result);
                return $rowcount;     
            }                   
        }        
    }
    else
    {
        return false;
    }
    
}

// return a value
function SQLGetValue($table,$column,$parameters="",$debug=false)
{
    $Q = "SELECT $column FROM $table $parameters";
    $result = mysql_query($Q);
    if($debug==true)
    {
        //setDebugMsg("SQL Query : $Q");
        echo $Q;
    }
    if($result)
    {
        while($rw = mysql_fetch_array($result))
        {
        return $rw[$column];
        }
    }
    else
    {
        return false;
    }
    
}

/*** FUnction Active UPDATE Using POST ***/
   function SQLUpdate($tablename,$condition=array(),$fields=array(),$debug=false)
    {
        $Q .= "UPDATE $tablename SET ";
        foreach($fields as $input=>$columns)
        {
            $Q .= "`$columns`='$_POST[$input]',";
            //$Q .= "$columns='$input',";
        }
        $trimmed = rtrim($Q,',');
        //$trimmed .= " WHERE $keyword='$value'";
        $trimmed .= " WHERE";
        foreach($condition as $where=>$value)
        {
            $trimmed .= " $where = '$value' AND";
        }
        $SQL .= rtrim($trimmed,'AND');
        if($debug==true)
        {
            setDebugMsg("SQL Query : $SQL");            
        } else {
            $result = mysql_query($SQL);
            return $result;   
        }
    }
    
    /*** FUnction Active UPDATE Using given value***/
   function SQLUpdate2($tablename,$condition=array(),$fields=array(),$debug=false)
    {
        $Q .= "UPDATE $tablename SET ";
        foreach($fields as $input=>$columns)
        {
            $Q .= "`$columns`='$input',";
            //$Q .= "$columns='$input',";
        }
        $trimmed = rtrim($Q,',');
        //$trimmed .= " WHERE $keyword='$value'";
        $trimmed .= " WHERE";
        foreach($condition as $where=>$value)
        {
            $trimmed .= " $where = '$value' AND";
        }
        $SQL .= rtrim($trimmed,'AND');
        if($debug==true)
        {
            echo "<pre>DEBUG SQL : $SQL</pre>";            
        } else {
            $result = mysql_query($SQL);
            return $result;   
        }
    }
    
    /*** New Functions for Update. Later deprecating the old functions ***/
   function SQLDoUpdate($tablename,$condition=array(),$fields=array(),$debug=false)
    {
        $Q .= "UPDATE $tablename SET ";
        foreach($fields as $columns=>$input)
        {
            $Q .= "`$columns`='$input',";
            //$Q .= "$columns='$input',";
        }
        $trimmed = rtrim($Q,',');
        //$trimmed .= " WHERE $keyword='$value'";
        $trimmed .= " WHERE";
        foreach($condition as $where=>$value)
        {
            $trimmed .= " `$where` = '$value' AND";
        }
        $SQL .= rtrim($trimmed,'AND');
        if($debug==true)
        {
            echo "<pre>DEBUG SQL : $SQL</pre>";            
        } else {
            $result = mysql_query($SQL);
            return $result;   
        }
    }
    
    // SQL Query
    function SQLQuery($table,$col="*",$parameters="",$debug=false){
        if($col){
            $sQ = "SELECT $col FROM $table $parameters";
        } else {
            $sQ = "SELECT * FROM $table $parameters";
        }
        if($debug==true){
            setDebugMsg("SQL Query : $sQ");
        }
        $result = mysql_query($sQ);
        return $result;
    }
    
    // SQL Insert Query
        function SQLInsert($tablename,$fields=array(),$debug=false)
    {
        $db = new DB();
        $db->db();
        $Q .= "INSERT INTO $tablename (";
        foreach($fields as $input=>$columns)
        {
            $Q .= "`$columns`,";
        }
        $trimmed = rtrim($Q,',');
        $trimmed .= ") VALUES (";
        foreach($fields as $input1=>$columns1)
        {
            $trimmed .= "'".($_POST[$input1])."',";
        }
        $SQL .= rtrim($trimmed,',');
        $SQL .= ")";
       if($debug==true)
       {
                 echo "<pre>SQL DEBUG : $SQL</pre>";
       } else {
           $result = mysql_query($SQL);
           return $result;
       }
    }
    
    // new SQL Insert Function
    function SQLDoInsert($tablename,$fields=array(),$type="",$debug=false){  

            $Q = "INSERT INTO $tablename (";
            foreach($fields as $cols=>$vals)
            {

                $Q .= "`$cols`, ";
            }
            $trimmed = rtrim($Q,', ');
            $trimmed .= ") VALUES (";
            foreach($fields as $cols1=>$val1)
            {
                if($type=="POST")
                {
                    $trimmed .= "\"".($_POST[$cols1])."\",";
                }
                else
                {
                    $trimmed .= "\"".($val1)."\",";
                }                
            }
            $SQL .= rtrim($trimmed,',');
            $SQL .= ")";
            if($debug==true)
            {
                        echo "<pre style='overflow: wrap'>SQL DEBUG : $SQL</pre>";
            } else {
                $result = mysql_query($SQL);
                return $result;
            }
    }
    
    function SQLInsert2($tablename,$fields=array(),$debug=false)
    {
        $db = new DB();
        $db->db();
        $Q .= "INSERT INTO $tablename (";
        foreach($fields as $input=>$columns)
        {
            $Q .= "`$columns`,";
        }
        $trimmed = rtrim($Q,',');
        $trimmed .= ") VALUES (";
        foreach($fields as $input1=>$columns1)
        {
            $trimmed .= "'".($input1)."',";
        }
        $SQL .= rtrim($trimmed,',');
        $SQL .= ")";
       if($debug==true)
       {
                 echo "<pre>SQL DEBUG : $SQL</pre>";
       } else {
           $result = mysql_query($SQL);
           return $result;
       }
    }
    
    // SQL Delete Query
    function SQLDelete($table,$condition,$debug=false){
        $Q = "DELETE FROM $table $condition";
        if($debug==true){
            echo "<pre>SQL DEBUG : $Q</pre>";
        } else {
            $result = mysql_query($Q);
            return $result;
        }
    }
    
    // SQL Lookup List
    function SQLLookup($data,$selectname,$valuecol,$textcol,$defaultval='',$selectparam='',$xtrarow='')
    {
        $select .= "<select name='$selectname' $selectparam>\n";
        if($xtrarow!='')
        {
            if(is_array($xtrarow)){
                foreach($xtrarow as $value=>$label){
                    $select .= "<option value='$value'>$label</option>";
                }
            } else {
            $select .= "<option value='0'>$xtrarow</option>";
            }
        }
        else
        {
            $select .= "";
        }
        while($row = mysql_fetch_array($data))
        {
            $value = $row[$valuecol];
            $text = $row[$textcol];
            if($defaultval==$value)
            {
                $selecthis='selected';
            }
            else
            {
                $selecthis='';
            }
            $select .= "<option value='$value' $selecthis>$text</option>\n";
        }
        $select .= "</select>\n";
        return $select;
    }
    
    function RSQLQuery($SQLQuery,$debug=false){
    
    // include the file
    include CONFIG."DB.Configs".EXT;
    
// start db configuration here
    $enablesync = $config['enableredundancy'];
    $dbhost = $config['hostlist'];
    $dbname = $config['dbname'];
    $dbuser = $config['dbuser'];
    $dbpass = $config['dbpass'];
    
    
    if($enablesync==1){
            foreach($dbhost as $dbserver){
                
                // Create Connection
                $conn = mysql_connect($dbserver, $dbuser,$dbpass);
                mysql_select_db($dbname,$conn);
                if($debug==true){
                      if($conn){
                    echo "<pre>DB Connection to $dbserver is OK<br /></pre>";
                        } else {
                            echo "DB Connection failed to $dbserver with $dbuser, $dbpass, $dbname";
                        }
                    echo "<pre>{$SQLQuery}</pre>";
                } else {
                // Execute Query
                mysql_query($SQLQuery,$conn);
                
                // Close Connection
                mysql_close($conn);
                }
            }
    }
}

// deprecated for version 1.0 just a replacement for TACACS Applications
function LibSQLQuery($SQLQuery,$debug=false){
    
    // include the file
    include CONFIG."DB.Configs".EXT;
    
// start db configuration here
    $enablesync = $config['enableredundancy'];
    $dbhost = $config['hostlist'];
    $dbname = $config['dbname'];
    $dbuser = $config['dbuser'];
    $dbpass = $config['dbpass'];
    
    
    if($enablesync==1){
            foreach($dbhost as $dbserver){
                
                // Create Connection
                $conn = mysql_connect($dbserver, $dbuser,$dbpass);
                mysql_select_db($dbname,$conn);
                if($debug==true){
                      if($conn){
                    echo "<pre>DB Connection to $dbserver is OK<br /></pre>";
                        } else {
                            echo "DB Connection failed to $dbserver with $dbuser, $dbpass, $dbname";
                        }
                    echo "<pre>{$SQLQuery}</pre>";
                } else {
                // Execute Query
                mysql_query($SQLQuery,$conn);
                
                // Close Connection
                mysql_close($conn);
                }
            }
    } else {
        mysql_query($SQLQuery);
        }
}

function get_assoc($result){
    if($result)
    {
        while($rw = mysql_fetch_assoc($result))
        {
            $data[] = $rw;
        }
        return $data;
    }
    else
    {
        return false;
    }
    
}

class Data {
    var $SetCSS;
    var $ResultSet;
    var $TableWidth;
    var $CellPadding = 0;
    var $CellSpacing = 0;
    var $SetTHProperties;
    var $SetTDProperties;
    var $SetTRProperties;
    var $HeaderCols = array();
    var $SetColSize = array();
    var $SetColStyle = array();
    var $EnableCount = 1;
    var $AltColor = array("#F6F6F6","#FFF");
    var $SetLinkID;
    var $LinkURL;
    var $striphtml = true;
    var $DataArray = array();
    var $IsCustomData = false;
    
    // *** function to display table grid view ***//
    function grid_view(){
        if($this->SetCSS){
            $styleid = "id='$this->SetCSS'";            
        } else {
            $styleid = "";
        }
        if($this->TableWidth){
            $width = "width='$this->TableWidth'";
        } else {
            $width = "";
        }
        if($this->SetLinkID){
            $linkid = "onClick=goURL('$this->LinkURL";
        } else {
            $linkid = "";
        }
        ?>
        <table <?php echo $width ?> <?php echo $styleid ?> cellpadding="<?php echo $this->CellPadding ?>" cellspacing="<?php echo $this->CellSpacing ?>">
            <tr>
                <?php
                if($this->EnableCount==1){
                    $count = 1;
                    ?>
                <th style="width: 30px" <?php echo $this->SetTHProperties ?>>No</th>
                <?php
                }
               foreach ($this->HeaderCols as $col=>$value){
               ?>
                <th <?php echo $this->SetTHProperties ?> style="width: <?php echo $this->SetColSize[$col] ?>"><?php echo $col ?></th>
                <?php
                 }                  
                ?>
            </tr>
            <tbody>
            <?php
            while($rw = mysql_fetch_array($this->ResultSet)){
                $color = ($count%2)?$this->AltColor[0]:$this->AltColor[1];
            ?>
            <tr <?php echo $this->SetTRProperties ?> <?php $datalink = $linkid;
                    if(is_array($this->SetLinkID)){
                        foreach($this->SetLinkID as $urlid){
                            $datalink .= $rw[$urlid]."/";
                        }
                        $datalink .= "')";
                    } else {
                            $datalink .= $rw[$this->SetLinkID]."/')";
                    }
                    echo $datalink;
                    ?> style="background-color: <?php echo $color ?>">
                <?php
                if($this->EnableCount==1){
                    ?>
                <td <?php echo $this->SetTDProperties ?>><?php echo $count ?></td>
                <?php
                }
                foreach($this->HeaderCols as $col=>$value){
                   if($this->IsCustomData==false){
                       if($this->striphtml==false){
                           $valuewr = $rw[$value];
                       } else {
                           $valuewr = strip_tags($rw[$value]);
                       }
                    ?>
                <td <?php echo $this->SetTDProperties ?>  style="<?php echo $this->SetColStyle[$col] ?>"><?php echo $valuewr ?></td>
                <?php
                   } else {
                       ?>
                <td <?php echo $this->SetTDProperties ?>  style="<?php echo $this->SetColStyle[$col] ?>"><?php echo $value ?></td>
                        <?php
                   }
                }
                ?>                
            </tr>
            <?php
            $count++;
            }
            ?>
            </tbody>
        </table>
        <?php
    }
}
?>