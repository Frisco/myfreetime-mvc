<?php
if (!defined('BASEDIR')) exit('<code>Forbidden Access</code>');
# Class : Controller
# Type : System
# Author : Sanusi Zainol Abidin
# Date : 15/03/2012

class Controller {
    
    protected $variables = array();

    private static $instance;
    
    function __construct() {
        
        include CONFIG."application.php";
        
        self::$instance =& $this;
        
        # Load the Template class
        $this->_template = new Template();
        
        # Load the System Library
        $syslib = $apps['system_libraries'];
        foreach($syslib as $classon)
        {
            if(file_exists(SYSLIB.$classon.".php"))
            {
                $this->$classon = new $classon();
            }
        }
                
        # Load all the class from the application configuration file : controllers, models, libraries
        # We become the Super Object
        $classarray = is_loaded();
        
        $loc = array(MODEL, CONTROLLER, LIBRARY);
                
        foreach( $loc as $path )
        {
            foreach($classarray as $var => $class)
            {
                # load the file if exist
                if (file_exists($path.$var.".php"))
                {
                    $this->$var =& load_apps_class( $path, $class);
                }
            }
            break;
        }
    }
    
    # Function to set variable arrays
    function set($name,$value)
    {
        $this->variables[$name] = $value;
    }
    
    # view loader to load view required in the controller
    function loadview($viewname,$auto_create=FALSE)
    {
        extract($this->variables);
        if(file_exists(VIEW.$viewname.".php")){
            include VIEW."$viewname.php";
        } 
        else 
        {
            if($auto_create === TRUE)
            {
                $this->create_view_file($viewname);
                include VIEW."$viewname.php";
            }
            else
            {
                load_msg("View $viewname Not Found","msg");
            }            
        }
    }
    
    # function to create view file
    private function create_view_file($viewname)
    {
        $viewname_trimmed = substr("$viewname", strpos($viewname, "/") + 1);
        $content = "<?php if (!defined('BASEDIR')) {exit('<code>Forbidden Access</code>');}?>\n"
                . "\n"
                . "\n"
                . "This is view file is automatically created from Controller/".  get_called_class() . ". View name is {$viewname_trimmed}";
        file_put_contents(VIEW.$viewname.".php", $content, FILE_APPEND | LOCK_EX);        
    }
}