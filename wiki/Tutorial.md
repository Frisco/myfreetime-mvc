Go back to [Main Content](https://bitbucket.org/ucishark1978/php-mvc/wiki/Introduction%20to%20PHP-MVC)
* * *
**Tutorial - Simple Inventory Application**

* Table ``items``
* Model ``Items`` filename is ``Items.php``
* Controller ``inventory`` filename is ``inventory.php``
* View ``list-inventory`` filename is ``list-inventory.php``

* * *

**Table ``items``**

Let's create a table first!
```
#!sql
CREATE TABLE
items (
   item_id INT NOT NULL AUTO_INCREMENT,
   item_title VARCHAR(100) NOT NULL,
   item_quantity INT NOT NULL,
   item_created_date DATE,
   PRIMARY KEY ( item_id )
);
```
And now insert some data into the table
```
#!sql
INSERT INTO items (item_title, item_quantity, item_created_date) VALUES
('Turbo Kit',5,'2015-12-01'),
('Radiator Hose',22,'2015-12-01'),
('Spark Plugs',111,'2015-12-04'),
('Air Filter',83,'2015-12-03');
```
And we are done in the table section. Let's start doing our first modelling.
* * *
**Model ``Items``**
This is how the code should looks like
```
#!php
class Items extends Models {

   # fetch all rows from the Items table
   function getAllRows()
   {
      return $this->findAll();
   }

   # fetch row from Items table by item_id
   function getRowByID($id,'item_id')
   {
      return $this->findbyid();
   }

   # create new items into the Items table
   function createItem($data = array())
   {
      return $this->create($data);
   }

   # edit existing items from the Items table
   function updateItem($id, $data = array())
   {
      $this->_conditions = array("WHERE" => "item_id = $id");
      return $this->update($id, $data);
   }

   # remove existing items from the Items table
   # currently `delete` method has yet to be introduced since most of my programming
   # is using either the data state is TRUE or FALSE. So we just run a normal query
   function removeItem($id)
   {
      $sql = "DELETE FROM items WHERE id = $id";
      return $this->exec_query($sql);
   }
}
```
