Go back to [Main Content](https://bitbucket.org/ucishark1978/php-mvc/wiki/Introduction%20to%20PHP-MVC)
* * *
The basic operation of this PHP-MVC is very simple. You separate each function into 3 separate files. As we all know, MVC stands for the Model-View-Controller. But how does it works here? Lets go through each and every function of it.
* * *
#Model#
Model is where all the database abstraction is happened. Usually what we do in here is where we define all the database queries and return whatever we needed back to the controller. PHP-MVC is simple enough. You can do the normal SQL queries or you can always do the so-called ORM style (which is not really completed in this framework yet).
* * *
#View#
The view is only the presentation layer. It contains 90% of the HTML syntax and how you want that specific page to be look like. The view is loaded via the Controller where we pass all the variables that we need in Controller and push it back to the view for the presentation. View is rendered based on the template that we have defined.
* * *
#Controller#
This is the main function of the PHP-MVC. Think of it as a central control of all the application that you want. It is simple. Example:-  
http://localhost:8080/inventory-apps/inventory/create/  
The sample URL above means:-  
http://localhost:8080/inventory-apps/{Controller-Class-Name}/{method-or-function-called}/  
Simple isn't it? Well, go through every Model, View and Controller wiki to know better of how to code them in PHP-MVC