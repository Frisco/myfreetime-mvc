Go back to [Main Content](https://bitbucket.org/ucishark1978/php-mvc/wiki/Introduction%20to%20PHP-MVC)
* * *
# HTML #

* [HTML::createform](#createform)
* [HTML::select](#select)



## HTML::createform($attributes)

Usage : Create a quick form for testing or application mock-up purpose

Example :

Controller Name : shipment

Model Name : Shipments

View Name : create-shipment

### Model ###
```
#!php
class Shipments extends Model {
     
    function createShipment($array=array())
    {
        return $this->create($array);
    }
}
```
The above Model create a function for creating new Shipment into the Shipment table.
* * *
### Controller ###
```
#!php
function create()
{
if(Security::f_server() === "POST")
        {
            $shipment_name = filter_input(INPUT_POST, "shipment_name");
            $arrival_date = filter_input(INPUT_POST, "arrival_date");
            $contact = filter_input(INPUT_POST, "contact");
            $array = array("shipment_name" => $shipment_name, "arrival_date" => $arrival_date, "contact" => $contact);
            $this->Shipments->createShipment($array);
            header("Location:" . BASE_URL . "shipment/");
        }
        else
        {
            $form_attributes = array(
                  "shipment_name" => array("type='text' placeholder='Enter Shipment Name' autofocus='true' class='form-control' required"),
                  "arrival_date" => array("type='text' placeholder='Enter Shipment Arrival Date (dd-mm-yyyy)' class='form-control' required"),
                  "contact" => array("type='text' placeholder='Enter Contact Number (60-xxxxxxxx)'"),
   );
            $this->set("form_attributes",$form_attributes);
            $this->loadview("shipment/create-shipment");
        }
}
```
The controller above define ```create``` function to be used when accessing URL shipment/create. It will check for any POST request available if not it will load the form which define in the else statement conditions. Take a look at the else statement of how to use the quick form to create a form display.
* * *
### View ###
```
#!php
<div class="row">
    <div class="col-lg-6">
    <?php echo HTML::createform($form_attributes); ?>
    </div>
</div>
```
The view above will load back all the attributes set in the controller and loaded it into the form for presentation/view by using the HTML::createform function.
* * *
## HTML::select($select_attributes) ##
Usage : Create a quick html select for testing or application mock-up purpose
```
#!php
function shipment()
    {        
        $disable_select = array(
            "properties" => array("name" => "shipment_status", "class" => "form-control"),
            "static_options" => array("Active" => 1, "Inactive" => 2),            
        );
        
        # options with SQL Query using PHP-MVC Framework
        $user_lookup = $this->Shipment->getAllUserList();
        $user_select = array(
            "properties" => array("name" => "userlist", "class" => "form-control"),
            # array key required : 
            # 'data' : define the Model function that will used to query the list 
            # 'value': define the table column name that will be used as a value 
            # 'label': define the table column name that will be used as a label
            "sql_options" => array("data" => $user_lookup, "value" => "userid", "label" => "username")
        );
        
        $form_attributes = array(
          "shipment_name" => array("type='text' class='form-contro' required placeholder='Please enter shipment name'"),
            HTML::select($disable_select), HTML::select($user_select),
        );
        
        $this->set("form_attributes",$form_attributes);
        $this->loadview("shipment/create-shipment");
    }
```
In a condition that you need to add static and sql options in the select, you can use the followings:-
```
#!php
$user_select = array(
            "properties" => array("name" => "userlist", "class" => "form-control"),
            "static_options" => array("Please select userlist" => ""),
            "sql_options" => array("data" => $user_lookup, "value" => "userid", "label" => "username")
        );
```
Please take note that the HTML library need to be loaded in the configs/application.php in system libraries array.