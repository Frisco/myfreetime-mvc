Go back to [Main Content](https://bitbucket.org/ucishark1978/php-mvc/wiki/Introduction%20to%20PHP-MVC)
* * *
# Model #
Model is used to defined all the SQL related and passed it back to the Controller. The naming convention for the Model is based on the scenario given below:-

Table name : shipment
Model name : Shipment


```
#!php

class Shipment extends Model {

  function getShipmentList()
  {
    return $this->findAll();
  }
}

```
The above tell the *getShipmentList()* method to retrieve all data rows from the shipment table.

* * *

## Retrieve Data ##
The below example shows how to use the Model to retrieve data
* findAll()
```
#!php

class Shipment extends Model {

  function getShipmentList()
  {
    return $this->findAll();
  }
}

```
The findAll() is the same as:-


```
#!SQL
SELECT * FROM shipment;

```
* find()
```
#!php

class Shipment extends Model {

  function getShipmentList()
  {
    $this->_debug = TRUE;
    $this->_columns = array("shipment.id","customer.name","items.descriptions");
    $this->_conditions = array("LEFT JOIN" => array (
                                               "customer ON customer.id = shipment.customer_id",
                                               "items ON items.id = shipment.items_id"),
                         "LIMIT" => 100);
    return $this->find();
  }
}

```
The above *find()* function can be use with the variables define as per below. The normal query for SQL will be
```
#!SQL
SELECT shipment.id,customer.name,items.descriptions FROM shipment 
LEFT JOIN customer ON customer.id = shipment.customer_id
LEFT JOIN items ON items.id = shipment.items_id
LIMIT 100
```
* * *

## Insert Data ##
The below example shows how to use the Model to retrieve data
```
#!php

class Shipment extends Model {

  function insertShipmentDetails($attributes=array())
  {
    return $this->create($attributes);
  }
}

```
The $attributes array example can be pass in the ***Controller*** as below example:-
```
#!php
$attributes = array("id"=>"110", "shipment_code"=>"SHIP-00-12", "contact_person"=>"Mr John Doe");
$this->Shipment->insertShipment($attributes);

```
The normal query for SQL will be
```
#!SQL
INSERT INTO Shipment (id, shipment_code, contact_person) 
VALUES (110,'SHIP-00-12','Mr John Doe');
```
* * *

## Update Data ##
The below example shows how to use the Model to update data
```
#!php

class Shipment extends Model {

  function updateShipmentDetails($id, $attributes=array())
  {
    $this->_conditions = array("WHERE" => "id = $id", 
                               "AND" => array("shipment_status = 'active'",
                                              "create_by = 'super_admin'")
    );
    return $this->update($attributes);
  }
}

```
The $attributes array example can be pass in the ***Controller*** as below example:-
```
#!php
$id = 110;
$attributes = array("shipment_code"=>"SHIP-00-13", "contact_person"=>"Mr John Die");
$this->Shipment->updateShipmentDetails($id, $attributes);
```
The normal query for SQL will be
```
#!SQL
UPDATE Shipment SET shipment_code = "SHIP-00-13", contact_person = "Mr John Die"
WHERE id = 110 AND shipment_status = 'active' AND created_by = 'super_admin';
```
* * *
## Retrieving Last Insert ID ##
The below example shows how to use the Model to retrieve last inserted id
```
#!php

class Shipment extends Model {

  function insertShipmentDetails($attributes=array())
  {
    $this->_return_id = "RETURNING id as last_insert_id";
    return $this->create($attributes);
  }
}
```
The last insert id value can be retrieve in the ***Controller*** as below example:-
```
#!php
$attributes = array("id"=>"110", "shipment_code"=>"SHIP-00-12", "contact_person"=>"Mr John Doe");
$exec_query = $this->Shipment->insertShipment($attributes);
$last_inserted_id = $exec_query[0]['last_insert_id'];
```
The normal query for SQL will be
```
#!SQL
INSERT INTO Shipment (id, shipment_code, contact_person) 
VALUES (110,'SHIP-00-12','Mr John Doe') RETURNING id as last_insert_id;
```
* * *